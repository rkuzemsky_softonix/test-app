import Vue from 'vue'
import {
  Button,
  Slider,
  Card,
  Icon,
  Table,
  TableColumn,
  Form,
  FormItem,
  Input
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

// configure language
locale.use(lang)

// import components
Vue.component(Button.name, Button)
Vue.component(Slider.name, Slider)
Vue.component(Card.name, Card)
Vue.component(Icon.name, Icon)
Vue.component(Table.name, Table)
Vue.component(TableColumn.name, TableColumn)
Vue.component(Form.name, Form)
Vue.component(FormItem.name, FormItem)
Vue.component(Input.name, Input)
